import React from 'react'
import Banner from '../components/Banner'
import Highlights from'../components/Highlights'
import { Container } from 'react-bootstrap'

export default function Home(){
	return (
		<React.Fragment>
			<Banner />
		    <Container>
			    <Highlights />
		    </Container>
	    </React.Fragment>
	)
}